"use strict";
const inputField = document.querySelector(".text-input");
const textBox = document.querySelector(".text");
const resultBox = document.querySelector(".result");
const resultList = document.querySelector(".score-list");
const resetButton = document.querySelector(".reset-button");

class Type {
    constructor() {
        this.cursorIndex = 0;
        this.corrected = [];
        this.finishAt = null;
        this.index = 0;
        this.isIncorrect = false;
        this.status = "not started";
        this.startAt = null;
        this.result = {
            speed: 0,
            accuracy: 0,
        };
        this.text = texts[0];
        this.incorrects = [];
    }
    #backspace() {
        if (!this.isIncorrect) return;

        const letters = document.querySelectorAll(".letter");
        const cursor = letters[this.index];
        const beforeCursor = letters[this.index - 1];
        const errorStart = this.index - 2;

        const isAllCorrected =
            !this.incorrects.includes(errorStart) ||
            (this.incorrects.includes(errorStart) &&
                this.corrected.includes(errorStart));

        if (this.status === "incorrect last") {
            this.#mark(cursor, "blink");
            this.status = "ongoing";
        } else {
            this.#mark(cursor, "untyped");
            this.#mark(beforeCursor, "blink");
            if (isAllCorrected) {
                this.isIncorrect = false;
            }
            this.#decrement();
        }
    }
    #calculateScore() {
        const duration = ((this.finishAt - this.startAt) / 1000) / 60;
        const charCounts = this.text.length;
        const totalErrors = [...new Set(this.incorrects)].length;
        const speed = (this.text.length / 5 / duration).toFixed(2);
        const accuracy = 100 - ((totalErrors / charCounts) * 100).toFixed(2);

        this.result.speed = speed;
        this.result.accuracy = accuracy;
    }

    #createParagraph() {
        const p = document.createElement("p");
        for (const idx in this.text) {
            const span = this.#createSpan(idx);
            p.appendChild(span);
        }
        return p;
    }
    #createSpan(idx) {
        const style = +idx === 0 ? "blink" : "untyped";
        const span = document.createElement("span");
        span.classList.add("letter");
        span.classList.add(style);
        span.innerText = this.text[idx];
        return span;
    }
    #decrement() {
        this.index--;
    }

    #finish() {
        this.status = "done";
        this.finishAt = new Date().getTime();
        this.#calculateScore();
        this.#showScore();
    }
    #hideScore() {
        resultBox.classList.add("hidden");
    }
    #increment() {
        this.index++;
    }

    init() {
        const randomIndex = this.#randomize();
        this.text = texts[randomIndex];
        inputField.value = "";
        inputField.focus();
        this.#reset();
        this.#render();

    }
    #mark(el, style) {
        el.classList.remove(...el.classList);
        el.classList.add("letter");
        el.classList.add(style);
    }
    #randomize() {
        const max = texts.length - 1;
        const min = 0;
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    #render() {
        const p = this.#createParagraph();
        textBox.innerHTML = "";
        textBox.appendChild(p);
    }
    #reset() {
        this.cursorIndex = 0;
        this.corrected = [];
        this.finishAt = null;
        this.incorrects = [];
        this.index = 0;
        this.isIncorrect = false;
        this.status = "not started";
        this.startAt = null;
        this.result = { speed: 0, accuracy: 0 };
        this.#hideScore();
    }
    #showScore() {

        resultList.innerHTML = "";
        for (let item in this.result) {
            const li = document.createElement("li");
            li.innerText = `${item}: ${this.result[item]}`
            resultList.appendChild(li);
        }
      
        resultBox.classList.remove("hidden");
    }
    #start() {
        this.status = "ongoing";
        this.startAt = new Date().getTime();
    }
    type(e) {
        if (this.status === "not started") this.#start();
        if (e.inputType === "deleteContentBackward") return this.#backspace();

        const letters = document.querySelectorAll(".letter");
        const cursor = letters[this.index];

        if (e.data) {
            if (this.index === this.text.length - 1) {
                if (this.isIncorrect) {
                    this.#mark(cursor, "incorrect-highlight");
                    this.incorrects.push(this.index);
                    this.status = "incorrect last";
                } else {
                    if (e.data === cursor.innerText) {
                        this.#mark(cursor, "typed");
                        this.#finish();
                    } else {
                        this.#mark(cursor, "incorrect-highlight");
                        this.status = "incorrect last";
                    }
                }
            } else {
                const afterCursor = letters[this.index + 1];

                this.#mark(afterCursor, "blink");
                if (this.isIncorrect) {
                    this.#mark(cursor, "incorrect");
                    this.incorrects.push(this.index);
                } else {
                    if (e.data === cursor.innerText) {
                        if (this.incorrects.includes(this.index)) {
                            this.#mark(cursor, "corrected");
                            this.corrected.push(this.index);
                        } else {
                            this.#mark(cursor, "typed");
                        }
                    } else {
                        this.#mark(cursor, "incorrect");
                        this.isIncorrect = true;
                        this.incorrects.push(this.index);
                    }
                }

                this.#increment();
            }
        }
    }
}

const type = new Type();

type.init();

inputField.addEventListener("input", (e) => type.type(e));
resetButton.addEventListener("click", () => type.init());
