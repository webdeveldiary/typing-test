const texts = [
    "Somewhere around.",

    "In 1968, Celia Green analyzed the main characteristics of such dreams, reviewing previously published literature on the subject and incorporating new data from participants of her own.",

    "She concluded that lucid dreams were a category of experience quite distinct from ordinary dreams and said they were associated with rapid eye movement sleep (REM sleep). Green was also the first to link lucid dreams to the phenomenon of false awakenings.",

    "In 1975, Dr Keith Hearne had the idea to exploit the nature of Rapid Eye Movements (REM) to allow a dreamer to send a message directly from dreams to the waking world. Working with an experienced lucid dreamer (Alan Worsley), he eventually succeeded in recording (via the use of an electrooculogram or EOG) a pre-defined set of eye movements signalled from within Worsley's lucid dream. This occurred at around 8 am on the morning of April 12, 1975.",

    "Hearne's EOG experiment was formally recognized through publication in the journal for The Society for Psychical Research. Lucid dreaming was subsequently researched by asking dreamers to perform pre-determined physical responses while experiencing a dream, including eye movement signals. In 1980, Stephen LaBerge at Stanford University developed such techniques as part of his doctoral dissertation. In 1985, LaBerge performed a pilot study that showed that time perception while counting during a lucid dream is about the same as during waking life.",

    "Lucid dreamers counted out ten seconds while dreaming, signaling the start and the end of the count with a pre-arranged eye signal measured with electrooculogram recording. LaBerge's results were confirmed by German researchers D. Erlacher and M. Schredl in 2004. In a further study by Stephen LaBerge, four subjects were compared either singing while dreaming or counting while dreaming. LaBerge found that the right hemisphere was more active during singing and the left hemisphere was more active during counting.",

    "Palangka Raya is the capital and largest city of the Indonesian province of Central Kalimantan. The city is situated between the Kahayan and the Sabangau rivers on the island of Borneo. As of the 2020 census, the city has a population of 293,500; the official estimate as at mid 2021 was 298,950. Palangka Raya is the largest city by land area in Indonesia (approximately four times the size of Jakarta).",  
    
    "Most of the area is forested (including protected forests, nature conservation areas, and Tangkiling Forest). It also has the highest Human Development Index rating of any city in Kalimantan.",

    "The city is the center of economic, governance, and education of Central Kalimantan province. It is a relatively new city, founded from a small Dayak village of Pahandut in 1957. The city was planned from the scratch and the construction was assisted by the Soviet military due to Sukarno's relation to Eastern Bloc at the time. Despite relatively developed infrastructure and high Human Development Index rating, the city suffers from environmental problems such as haze, forest fires, and floods.",

    "The history of psychology of programming dates back to late 1970s and early 1980s, when researchers realized that computational power should not be the only thing to be evaluated in programming tools and technologies, but also the usability from the users.",

    "In the first Workshop on Empirical Studies of Programmers, Ben Shneiderman listed several important destinations for researchers. These destinations include refining the use of current languages, improving present and future languages, developing special purpose languages, and improving tools and methods.",

    "Two important workshop series have been devoted to psychology of programming in the last two decades: the Workshop on Empirical Studies of Programmers (ESP), based primarily in the US, and the Psychology of Programming Interest Group Workshop (PPIG), having a European character.",

    "ESP has a broader scope than pure psychology in programming, and on the other hand, PPIG is more focused in the field of PoP. However, PPIG workshops and the organization PPIG itself is informal in nature, It is group of people who are interested in PoP that comes together and publish their discussions. ",
];
